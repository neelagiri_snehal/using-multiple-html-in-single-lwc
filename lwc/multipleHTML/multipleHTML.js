import { LightningElement, track} from 'lwc';
import alternateHTML from './multipleHTMLHelper.html';

export default class MultipleHTML extends LightningElement {
    @track displayAlternateHTML = false;

    render(){
        //If displayAlternateHTML is true return alternateHTML template
        if(this.displayAlternateHTML){
            return alternateHTML;
        }
    }
}